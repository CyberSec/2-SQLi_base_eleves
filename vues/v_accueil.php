<!DOCTYPE html>
<html lang="fr">
  <head>
<?php include("vues/include/_entete.inc.php"); ?>
  </head>
  <body>
    <?php include("vues/include/_menu.inc.php"); ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-md-12">
                <h2>Base élèves.</h2>
                <p class="lead">
                    Cette application web permet de gérer les élèves, leur classe,
                    les professeurs et leur spécialité respective.
                </p>
            </div>
        </div>
        <div class="panel panel-accueil" style="padding:15px 15px;">
            <p>
                Elle offre les services suivants :
            </p>
            <ul>
                <li>Gérer la spécialité des professeurs et la classe dont ils sont professeur principal.</li>
                <li>Gérer les élèves et leur classe.</li>
            </ul>
        </div>
    </div>
  </body>
</html>
