<!DOCTYPE html>
<html lang="fr">
  <head>
<?php include("vues/include/_entete.inc.php"); ?>
  </head>
  <body>
    <?php include("vues/include/_menu.inc.php"); ?>
    <div class="container">
        <?php include("vues/include/_erreurs.php"); ?>
        <div class="panel panel-amap">
            <div class="panel-heading text-center">
                <strong>Authentification</strong>
            </div>
            <h5>Veuillez SVP entrer vos identifiants dans les champs ci-après :</h5>
            <form action="index.php?uc=gererLogin" method="post"
            role="form" class="form-horizontal" style="padding:10px 15px;">
                <div class="form-group">
                    <label for="login"
                    class="col-sm-3 control-label">Votre login* :</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control"
                        id="login" placeholder="Identifiant de connexion"
                        name="_login"
                        value="<?php echo (isset($login)) ? $login : ''; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="motdepasse"
                    class="col-sm-3 control-label">Votre mot de passe* :</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control"
                        id="motdepasse" placeholder="Mot de passe"
                        name="_motdepasse" maxlength="25"
                        value="<?php echo (isset($motdepasse)) ? $motdepasse : ''; ?>">
                    </div>
                </div>
                <button type="submit" class="btn btn-amap">Valider</button>
                <button type="reset" class="btn btn-amap">Annuler</button>
            </form>

            <div class="panel-footer">
                <a href="/">
                    <span class="glyphicon glyphicon-circle-arrow-left"></span>
                    Retour
                </a>
            </div>
        </div>
    </div>
  </body>
</html>
