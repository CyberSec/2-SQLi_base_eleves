<!DOCTYPE html>
<html lang="fr">
  <head>
<?php include("vues/include/_entete.inc.php"); ?>
  </head>
  <body>
    <?php include("vues/include/_menu.inc.php"); ?>
    <div class="container">
        <div class="panel panel-amap">
            <div class="panel-heading text-center">
                <strong><?php echo $monEleve->getPrenom(), " ", $monEleve->getNom(); ?></strong>
            </div>
            <dl class="dl-horizontal" style="padding:5px 15px;">
                <dt>Id :</dt>
                <dd><?php echo $monEleve->getId(); ?></dd>
                <dt>Nom :</dt>
                <dd><?php echo $monEleve->getNom(); ?></dd>
                <dt>Prénom :</dt>
                <dd><?php echo $monEleve->getPrenom(); ?></dd>
                <dt>Tel. responsable :</dt>
                <dd><?php echo $monEleve->getTelResp(); ?></dd>
                <dt>Classe :</dt>
                <dd><?php echo $monEleve->getClasse()->getLibelle(); ?></dd>
            </dl>
            <div class="panel-footer">
                <a href="index.php?uc=gererEleves">
                    <span class="glyphicon glyphicon-circle-arrow-left"></span>
                    Retour
                </a>
            </div>
        </div>
    </div>
  </body>
</html>
