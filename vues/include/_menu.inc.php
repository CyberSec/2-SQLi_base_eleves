<!--  Gestion de la barre de navigation -->
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#barre-de-navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Base élèves</a>
            </div>
            <div class="collapse navbar-collapse"  id="barre-de-navigation">
                <ul class="nav navbar-nav">
                    <li class="<?php echo $classAccueil; ?>">
                        <a href="index.php"><span class="glyphicon glyphicon-home"></span> Accueil</a>
                    </li>
                    <li class="<?php echo $classClasses; ?>">
                        <a href="index.php?uc=gererClasses">
                            <span class="glyphicon glyphicon-pencil"></span> Gestion classes
                        </a>
                    </li>
                    <li class="<?php echo $classEleves; ?>">
                        <a href="index.php?uc=gererEleves">
                            <span class="glyphicon glyphicon-user"></span> Gestion élèves
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
