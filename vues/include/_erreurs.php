<!-- Affichage des messages d'erreur -->
        <div class="has-error">
        <?php if (isset($_REQUEST['erreurs'])) : ?>
            <ul>
            <?php foreach ($_REQUEST['erreurs'] as $erreur) : ?>
              <li>
                  <span class="help-block"><?php echo $erreur; ?></span>
              </li>
            <?php endforeach; ?>
            </ul>
        <?php endif; ?>
        </div>
