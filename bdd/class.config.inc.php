<?php
/**
 * @author moulinux
 * @version 0.1
 * @copyright 2019 par moulinux
 * @license AGPLv3, voir la LICENCE pour plus d'informations
 */
namespace App\Bdd;

/**
 * Définition de la classe Config permettant de gérer les constantes de l'application.
 */
class Config
{
    const DB_NAME = 'eleves';       // nom de la base de données
    const USER = 'moulinux';        // propriétaire ou utilisateur
    const PASSWD = 'secret';        // mot de passe d'accès
    const HOST = '127.0.0.1';       // nom ou adresse IP du serveur
    const SQLITE_DATABASE = 'eleves.db';
    const SCRIPT = 'eleves.sql';

    const SQLITE_DATABASE_URI = 'sqlite:'.Config::SQLITE_DATABASE;
    const MYSQL_DATABASE_URI = 'mysql:host='.Config::HOST.';'.
                                'dbname='.Config::DB_NAME;
    const PGSQL_DATABASE_URI = 'pgsql:host='.Config::HOST.';'.
                                'dbname='.Config::DB_NAME;
    const DATABASE_URI = Config::SQLITE_DATABASE_URI;
}
