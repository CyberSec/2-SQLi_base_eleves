Ce programme permet d'aborder les révisions en début de deuxième année.
La présente itération comporte des vulnérabilités qui permettent notamment
d'étudier les **injections SQL**.
L'application permet de gérer un système éducatif réduit.

# Environnement de développement
Pour initialiser l'application dans un environnement de développement.

Positionnez-vous dans votre répertoire de travail :
```
mkdir -p $HOME/prog
cd $HOME/prog
```

Clonez le projet (exemple ci-dessous en SSH) :
```
git clone git@framagit.org:CyberSec/2-SQLi_base_eleves.git
```

Vous pouvez enfin créer une base locale sqlite, pour la déplacer ensuite dans le dossier racine :
```
cd base_eleves/bdd
php createDb.php
mv eleves.db ../.
cd ..
```

> Vous pouvez bien sûr configurer une autre base de données via le fichier
`bdd/class.config.inc.php`.

Enfin, vous lancez le serveur de développement de PHP :
```
php -S 127.0.0.1:8000
```

> Pré-requis sur le système. Sur une distribution GNU/Linux Debian, un
certain nombre de paquets devront être installés sur le système à l'aide
de la commande `apt -y install php-cli php-sqlite3 php-codesniffer`

> Le respect du style du code a été testé à l'aide la commande :
`phpcs --standard=PSR2 index.php`

# Exploitation pédagogique

## Formulaire d'authentification
Après avoir déployé l'application **base-eleves** dans un environnement de production,
on peut demander aux apprenants d'accéder à la liste des classes, sans connaître donc
les identifiants enregistrés. Pour exploiter la faille de sécurité, les
apprenants ont accès au code de source de l’application en production.

## Sqlmap
Après une introduction au logiciel *open source* [sqlmap], permettant d'identifier
et d'exploiter les injections SQL, on peut commencer par mettre en œuvre
le logiciel à partir de l'application [sqlilabs].

On peut notamment s'appuyer sur cet article de [kali-linux] et également
de cette [présentation avancée][misc-062] de sqlmap paru dans le n°62 de
GLMF MISC (disponible sous licence Creative Commons BY-NC-ND FR).

## Mise en application
Toujours en s'appuyant sur une version de l'application **base-eleves**
déployée dans un environnement de production, on peut alors demander de :

* Déterminer le nom de la base de données courante, le nom de l'utilisateur courant et si cet utilisateur est administrateur de la base de données.
* Déterminer le nom des utilisateurs qui peuvent s’authentifier sur l’application et leur mot de passe. Ce mot de passe apparaît-il en clair ?
* Déterminer si vous pouvez accéder au système de fichiers du serveur en lecture.
* Déterminer si vous pouvez accéder au système de fichiers du serveur en écriture.
* Cloner le projet dédié et de corriger la vulnérabilité sans utiliser les requêtes paramétrées (même si c’est bien sûr une bonne pratique, ce n’est pas indispensable ici).

> Des vulnérabilités peuvent être exploitées à la fois pour des requêtes
de type `GET` et de type `POST`.


[sqlmap]: https://sqlmap.org/
[sqlilabs]: https://github.com/Rinkish/Sqli_Edited_Version
[kali-linux]: https://www.kali-linux.fr/conseil/sqlmap-part-2-utilisation-avancee
[misc-062]: https://connect.ed-diamond.com/MISC/misc-062/utilisation-avancee-de-sqlmap
