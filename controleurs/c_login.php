<?php
$titre = "Accueil";
$classAccueil = "active";
$classClasses = "";
$classEleves = "";

if (!empty($_POST)) {
    $echec = false;
    // Tester la validité des champs
    if ($_POST['_login'] != '') {
        $login = $_POST['_login'];
    } else {
        ajouterErreur("Le champ login doit être renseigné !");
        $echec = true;
    }
    if ($_POST['_motdepasse'] != '') {
        $motdepasse = $_POST['_motdepasse'];
    } else {
        ajouterErreur("Le champ mot de passe doit être renseigné !");
        $echec = true;
    }
    if (!$echec) {
        if ($pdo->verifierId($login, $motdepasse)) {
            $_SESSION['login'] = $login;
            header('Location: index.php?uc=gererClasses');
            exit;
        } else {
            ajouterErreur("Erreur d'identification !");
        }
    }
}
include("vues/v_login.php");
