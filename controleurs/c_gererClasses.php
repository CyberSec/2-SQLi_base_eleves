<?php
$action = null;
$id = null;
if (isset($_REQUEST['action'])) {
    $action = $_REQUEST['action'];
}
if (isset($_REQUEST['id'])) {
    $id = $_REQUEST['id'];
}
switch ($action) {
    case 'detail':
        $maClasse = $pdo->obtenirDetailClasse(intval($id));
        if ($maClasse) {
            $titre = "Détail de la classe n°".$id;
            $classAccueil = "";
            $classClasses = "active";
            $classEleves = "";
            include("vues/Classe/v_detailClasse.php");
            break;
        }
        ajouterErreur("$id, n'est pas un identifiant de classe connu dans la base");
        // pas de break, afin de basculer sur le traitement par défaut
    default:
        $titre = "Liste des classes";
        $classAccueil = "";
        $classClasses = "active";
        $classEleves = "";
        $lesClasses = $pdo->obtenirClasses();
        include("vues/Classe/v_listeClasse.php");
        break;
}
