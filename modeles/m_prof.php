<?php declare(strict_types=1);
/**
 * @author moulinux
 * @version 0.1
 * @copyright 2019 par moulinux
 * @license AGPLv3, voir la LICENCE pour plus d'informations
 */
namespace App\Models;

/**
 * Définition de la classe métier représentant un prof.
 */
class Prof extends Personne
{
    /**
     * @var Specialite $specialite
     * Spécialité du professeur
     */
    private $specialite;

    /**
     * Constructeur de la classe
     */
    public function __construct(
        int $unId = null,
        string $unNom = null,
        string $unPrenom = null,
        Specialite $uneSpecialite = null
    ) {
        parent::__construct($unId, $unNom, $unPrenom);
        if ($uneSpecialite) {
            $this->setSpecialite($uneSpecialite);
        }
    }

    /**
     * Accesseur de la spécialité
     * @return Specialite Instance de la classe Specialite liée au professeur
     */
    public function getSpecialite(): Specialite
    {
        return $this->specialite;
    }

    /**
     * Mutateur de la spécialité
     * @param Specialite $uneSpecialite Instance de la classe Specialite à lier au professeur
     */
    public function setSpecialite(Specialite $uneSpecialite)
    {
        $this->specialite = $uneSpecialite;
    }
}
