<?php declare(strict_types=1);
/**
 * @author moulinux
 * @version 0.1
 * @copyright 2019 par moulinux
 * @license AGPLv3, voir la LICENCE pour plus d'informations
 */
namespace App\Models;

/**
 * Définition de la classe métier représentant une personne.
 */
class Personne
{
    /**
     * @var int $id
     * Identifiant de la personne
     */
    private $id;
    
    /**
     * @var string $nom
     * Nom de la personne
     */
    private $nom;
    
    /**
     * @var string $prenom
     * Prénom de la personne
     */
    private $prenom;

    /**
     * Constructeur de la classe
     */
    public function __construct(int $unId = null, string $unNom = null, string $unPrenom = null)
    {
        if ($unId) {
            $this->setId($unId);
        }
        if ($unNom) {
            $this->setNom($unNom);
        }
        if ($unPrenom) {
            $this->setPrenom($unPrenom);
        }
    }
    
    /**
     * Accesseur de l'identifiant
     * @return int Identifiant de la personne
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Accesseur du nom
     * @return string Nom de la personne
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * Accesseur du prénom
     * @return string Prénom de la personne
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * Mutateur de l'identifiant
     * @param int $unId Identifiant de la personne
     */
    public function setId(int $unId)
    {
        $this->id = $unId;
    }

    /**
     * Mutateur du nom
     * @param string $unNom Nom de la personne
     */
    public function setNom(string $unNom)
    {
        $this->nom = $unNom;
    }

    /**
     * Mutateur du prénom
     * @param string $unPrenom Prénom de la personne
     */
    public function setPrenom(string $unPrenom)
    {
        $this->prenom = $unPrenom;
    }
}
