<?php declare(strict_types=1);
/**
 * @author moulinux
 * @version 0.1
 * @copyright 2019 par moulinux
 * @license AGPLv3, voir la LICENCE pour plus d'informations
 */
namespace App\Models;

/**
 * Définition de la classe métier représentant une spécialité.
 */
class Specialite
{
    /**
     * @var int $id
     * Identifiant de la spécialité
     */
    private $id;
    
    /**
     * @var string $libelle
     * Libellé de la spécialité
     */
    private $libelle;

    /**
     * Constructeur de la classe
     */
    public function __construct(int $unId, string $unLibelle)
    {
        $this->setId($unId);
        $this->setLibelle($unLibelle);
    }
    
    /**
     * Accesseur de l'identifiant
     * @return int Identifiant de la spécialité
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Accesseur du libellé
     * @return string Libellé de la spécialité
     */
    public function getLibelle(): string
    {
        return $this->libelle;
    }

    /**
     * Mutateur de l'identifiant
     * @param int $unId Identifiant de la spécialité
     */
    public function setId(int $unId)
    {
        $this->id = $unId;
    }

    /**
     * Mutateur du libellé
     * @param string $unLibelle Libellé de la spécialité
     */
    public function setLibelle(string $unLibelle)
    {
        $this->libelle = $unLibelle;
    }
}
